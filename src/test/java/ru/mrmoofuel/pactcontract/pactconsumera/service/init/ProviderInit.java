package ru.mrmoofuel.pactcontract.pactconsumera.service.init;

import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

import static ru.mrmoofuel.pactcontract.pactconsumera.service.PactProviderServiceTest.PROVIDER_CONFIG;

/**
 * @author dmitriy
 * @since 30.05.19
 */
public class ProviderInit implements ApplicationContextInitializer<ConfigurableApplicationContext> {
    /**
     * Initialize the given application context.
     *
     * @param applicationContext the application to configure
     */
    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertyValues.of(
                "provider.hello-url=http://localhost:"
                + PROVIDER_CONFIG.getPort() + "/provider/hello",
                "provider.chat-url=http://localhost:"
                        + PROVIDER_CONFIG.getPort() + "/provider/chat")
                .applyTo(applicationContext.getEnvironment());
    }
}
