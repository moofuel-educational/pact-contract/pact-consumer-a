package ru.mrmoofuel.pactcontract.pactconsumera.service;

import au.com.dius.pact.consumer.ConsumerPactBuilder;
import au.com.dius.pact.consumer.PactVerificationResult;
import au.com.dius.pact.model.MockProviderConfig;
import au.com.dius.pact.model.RequestResponsePact;
import lombok.RequiredArgsConstructor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import ru.mrmoofuel.pactcontract.pactconsumera.configuration.RestTemplateConfiguration;
import ru.mrmoofuel.pactcontract.pactconsumera.domain.dto.HelloDto;
import ru.mrmoofuel.pactcontract.pactconsumera.domain.dto.MessageDto;
import ru.mrmoofuel.pactcontract.pactconsumera.service.init.ProviderInit;

import static au.com.dius.pact.consumer.ConsumerPactRunnerKt.runConsumerTest;
import static io.pactfoundation.consumer.dsl.LambdaDsl.newJsonBody;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author dmitriy
 * @since 29.05.19
 */
@RequiredArgsConstructor
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {RestTemplateConfiguration.class,
        RestTemplateAutoConfiguration.class, PactProviderService.class},
        webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ContextConfiguration(initializers = ProviderInit.class)
public class PactProviderServiceTest {

    public static final MockProviderConfig PROVIDER_CONFIG = MockProviderConfig.createDefault();
    public static final String PACT_CONSUMER_A = "pact-consumer-a";
    public static final String PACT_PROVIDER = "pact-provider";
    public static final String HELLO = "Hello";

    @Autowired
    private PactProviderService providerService;

    @Test
    public void getGreetingsFromProvider() {
        final RequestResponsePact requestResponsePact = ConsumerPactBuilder.consumer(PACT_CONSUMER_A)
                .hasPactWith(PACT_PROVIDER)
                .uponReceiving("Greeting request")
                .matchPath("/provider/hello")
                .method(HttpMethod.GET.name())
                .willRespondWith()
                .body(newJsonBody(root ->
                        root.stringMatcher("hello", HELLO))
                        .build())
                .toPact();

        final PactVerificationResult pactVerificationResult =
                runConsumerTest(requestResponsePact, PROVIDER_CONFIG, (mockServer, pactTestExecutionContext) -> {
                    final HelloDto greetingsFromProvider = providerService.getGreetingsFromProvider();
                    assertThat(greetingsFromProvider)
                            .isNotNull();
                    final String hello = greetingsFromProvider.getHello();
                    assertThat(hello)
                            .isNotBlank()
                            .isEqualTo("Hello");
                });
        checkResult(pactVerificationResult);
    }

    @Test
    public void chatWithProvider() {
        final RequestResponsePact requestResponsePact = ConsumerPactBuilder
                .consumer(PACT_CONSUMER_A)
                .hasPactWith(PACT_PROVIDER)
                .uponReceiving("Chat with provider")
                .matchPath("/provider/chat")
                .method(HttpMethod.POST.name())
                .body(newJsonBody(root ->
                        root.stringMatcher("message", "lol"))
                        .build())
                .willRespondWith()
                .status(HttpStatus.OK.value())
                .body(newJsonBody(root ->
                        root.stringMatcher("message", "Hello"))
                        .build())
                .toPact();

        final PactVerificationResult pactVerificationResult =
                runConsumerTest(requestResponsePact, PROVIDER_CONFIG, (mockServer, pactTestExecutionContext) -> {
                    final MessageDto answer = providerService.chatWithProvider("lol");
                    assertThat(answer)
                            .isNotNull();
                    final String hello = answer.getMessage();
                    assertThat(hello)
                            .isNotBlank()
                            .isEqualTo(HELLO);
                });
        checkResult(pactVerificationResult);
    }

    @Test
    public void chatWIthProviderButHeDoesntKnowABoutUs() throws InterruptedException {
        Thread.sleep(2000);
        final RequestResponsePact requestResponsePact = ConsumerPactBuilder
                .consumer(PACT_CONSUMER_A)
                .hasPactWith(PACT_PROVIDER)
                .uponReceiving("He doesnt know about us")
                .matchPath("/provider/chat")
                .method(HttpMethod.POST.name())
                .body(newJsonBody(root ->
                        root.stringType("message", "Privet"))
                        .build())
                .willRespondWith()
                .status(HttpStatus.OK.value())
                .body(newJsonBody(root ->
                        root.stringMatcher("message", "wat"))
                        .build())
                .toPact();

        final PactVerificationResult pactVerificationResult =
                runConsumerTest(requestResponsePact, PROVIDER_CONFIG, (mockServer, pactTestExecutionContext) -> {
                    final MessageDto privet = providerService.chatWithProvider("Privet");
                    assertThat(privet)
                            .isNotNull();
                    final String message = privet.getMessage();
                    assertThat(message)
                            .isNotBlank()
                            .isEqualTo("wat");
                });
        checkResult(pactVerificationResult);
    }

    private void checkResult(PactVerificationResult result) {
        if (result instanceof PactVerificationResult.Error) {
            throw new RuntimeException(((PactVerificationResult.Error) result).getError());
        }
        assertThat(PactVerificationResult.Ok.INSTANCE)
                .isEqualTo(result);
    }
}