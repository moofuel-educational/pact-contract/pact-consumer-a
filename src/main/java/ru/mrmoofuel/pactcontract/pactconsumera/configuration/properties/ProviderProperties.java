package ru.mrmoofuel.pactcontract.pactconsumera.configuration.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author dmitriy
 * @since 29.05.19
 */
@Data
@ConfigurationProperties(prefix = "provider")
public class ProviderProperties {

    private String helloUrl;
    private String chatUrl;
}
