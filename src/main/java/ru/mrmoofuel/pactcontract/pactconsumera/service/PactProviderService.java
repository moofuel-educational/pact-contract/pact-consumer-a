package ru.mrmoofuel.pactcontract.pactconsumera.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ru.mrmoofuel.pactcontract.pactconsumera.configuration.properties.ProviderProperties;
import ru.mrmoofuel.pactcontract.pactconsumera.domain.dto.HelloDto;
import ru.mrmoofuel.pactcontract.pactconsumera.domain.dto.MessageDto;

/**
 * @author dmitriy
 * @since 29.05.19
 */
@Slf4j
@Service
@RequiredArgsConstructor
@EnableConfigurationProperties(ProviderProperties.class)
public class PactProviderService {

    private final RestTemplate restTemplate;
    private final ProviderProperties providerProperties;

    public HelloDto getGreetingsFromProvider() {

        final ResponseEntity<HelloDto> response = this.restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(providerProperties.getHelloUrl())
                        .build().toUri(),
                HttpMethod.GET,
                null,
                HelloDto.class);
        final HelloDto body = response.getBody();
        Assert.notNull((body), "Body must not be empty!");
        log.info("Here is how provider greets us: {}", body.getHello());
        return body;
    }

    public MessageDto chatWithProvider(String lol) {
        final MessageDto message = new MessageDto();
        message.setMessage(lol);
        final ResponseEntity<MessageDto> exchange = this.restTemplate.exchange(
                UriComponentsBuilder.fromHttpUrl(providerProperties.getChatUrl())
                        .build().toUri(),
                HttpMethod.POST,
                new HttpEntity<>(message),
                MessageDto.class
        );
        final MessageDto answer = exchange.getBody();
        Assert.notNull(answer, " Answer must not be null");
        return answer;
    }
}
