package ru.mrmoofuel.pactcontract.pactconsumera.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dmitriy
 * @since 29.05.19
 */
@Data
@NoArgsConstructor
public class HelloDto {

    private String hello;

}
