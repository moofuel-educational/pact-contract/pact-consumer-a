package ru.mrmoofuel.pactcontract.pactconsumera.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author dmitriy
 * @since 31.05.19
 */
@Data
@NoArgsConstructor
public class MessageDto {
    private String message;
}
