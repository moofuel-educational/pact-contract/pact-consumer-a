package ru.mrmoofuel.pactcontract.pactconsumera;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PactConsumerAApplication {

    public static void main(String[] args) {
        SpringApplication.run(PactConsumerAApplication.class, args);
    }

}
